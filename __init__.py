# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration, request, move

def register():
    Pool.register(
        configuration.Configuration,
        move.Move,
        request.Requisition,
        request.RequisitionLine,
        request.RequisitionQuotation,
        request.CreateQuotationStart,
        request.SelectProductAsk,
        module='requisition', type_='model')
    Pool.register(
        request.CreatePurchase,
        request.CreateShipment,
        request.CreateQuotation,
        module='requisition', type_='wizard')
    Pool.register(
        module='requisition', type_='report')
