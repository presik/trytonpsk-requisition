# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import datetime
from decimal import Decimal
from functools import partial
from itertools import groupby

from trytond.config import config
from trytond.exceptions import UserError
from trytond.ir.attachment import AttachmentCopyMixin
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, Get, If, In, Or
from trytond.tools import sortable_values
from trytond.transaction import Transaction
from trytond.wizard import Button, StateTransition, StateView, Wizard

if config.getboolean('attachment', 'filestore', default=True):
    file_id = 'file_id'
    store_prefix = config.get('attachment', 'store_prefix', default=None)
else:
    file_id = None
    store_prefix = None

from trytond.modules.company.model import CompanyValueMixin

STATES = {
    'readonly': Eval('state') != 'draft',
    }
DEPENDS = ['state']

TIPO = [
    ('store', 'Store'),
    ('purchase', 'Purchase'),
    ]


class Requisition(Workflow, ModelSQL, ModelView):
    "Requisition"
    __name__ = 'requisition'
    num_orden = fields.Char('No. Orden',
        states=STATES, depends=DEPENDS)
    id_interno = fields.Char('ID Interno',
        states=STATES, depends=DEPENDS)
    requisition_date = fields.Date('Requisition Date')
    destination = fields.Char('Destination',
        states=STATES, depends=DEPENDS)
    user_origin = fields.Many2One('res.user', 'User Request')
    requisition_line = fields.One2Many('requisition.line', 'requisition', 'Requisition Line')
    comment = fields.Text('Description',
        states=STATES, depends=DEPENDS)
    from_location = fields.Many2One(
        'stock.location', "From Location",
        states=STATES,
        depends=DEPENDS,
        domain=[('type', '=', 'warehouse')])

    to_location = fields.Many2One(
        'stock.location', "To Location",
        states=STATES,
        depends=DEPENDS,
        domain=[('type', '=', 'customer')])
    purchase = fields.Function(fields.Many2One('purchase.purchase',
        'Purchase'), 'get_purchase', searcher='search_purchase')
    company = fields.Many2One('company.company', 'Company', required=True,

          domain=[
              ('id', If(In('company', Eval('context', {})), '=', '!='),
               Get(Eval('context', {}), 'company', 0)),
          ])

    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states=STATES, depends=DEPENDS, required=True)
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', states=STATES, depends=DEPENDS, domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])

    party = fields.Many2One('party.party', 'Party', states=STATES,
        depends=DEPENDS)

    type = fields.Selection([
        ('store', "Store"),
        ('purchase', "Purchase"),
                ], "Type", required=True)

    user_processes = fields.Many2One('res.user', 'User Processes')
    state = fields.Selection([
            ('purchased', "Purchased"),
            ('waiting', "Waiting"),
            ('quotation', "Quotation"),
            ('approved', "Approved"),
            ('rejected', "Rejected"),
            ('purchase_approval', "Purchase Approval"),
            ('done', "Done"),
            ('draft', "Draft"),
            ('cancelled', "Cancelled"),
            ('exception', "Exception"),
            ('process', 'Process'),
            ], "State", required=True, readonly=True)

    state_purchase = fields.Selection([
        ('', ''),
        ('pending', "Pending"),
        ('done', "Done")],
        "State Purchase",
        readonly=True,
    )
    state_shipment = fields.Selection([
        ('', ''),
        ('pending', "Pending"),
        ('done', "Done")],
        "State Shipment",
        readonly=True,
    )

    @classmethod
    def __setup__(cls):
        super(Requisition, cls).__setup__()
        cls._order[0] = ('id', 'DESC')
        cls._buttons.update({
            'create_purchase': {
                'icon': 'tryton-add',
                'invisible': ~Eval('state').in_(['approved']),
                # 'invisible': Or((Eval('state') != 'purchase_approval'),(Eval('type') != 'purchase'))
                # 'readonly': Not(Eval('current_user', []).contains(Eval('context',{}).get('groups', ['Requisition Administration']))),
                'depends': ['current_user', 'state'],
                },
            'create_shipment': {
                'icon': 'tryton-add',
                'invisible': ~Eval('state').in_(['approved']),
                # 'invisible': Or((Eval('state') != 'approved'),(Eval('type') != 'store'))
                },
            'waiting': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state'],
                },
            'purchase_approval': {
                'invisible': ~Eval('state').in_(['waiting']),
                'depends': ['state'],
            },
            'quote': {
                'invisible': Or((Eval('state') != 'approved'), (Eval('type') != 'purchase')),
                'depends': ['state'],
            },
            'approved': {
                'invisible': Eval('state').in_(['draft', 'purchase_approval', 'approved']),
                'depends': ['state'],
            },
            })

        cls._transitions |= set((
            ('draft', 'waiting'),
            ('waiting', 'approved'),
            ('approved', "quotation"),
            ('quotation', 'purchase_approval'),
            ('quotation', 'process'),
            ('process', 'purchase_approval'),
            ('purchase_approval', 'rejected'),
            ('purchase_approval', 'quotation'),
            ('waiting', 'rejected'),
            ('rejected', 'draft'),
            ('draft', 'cancelled'),
        ))

    @staticmethod
    def default_type():
        return 'store'

    @fields.depends('party', 'to_location')
    def on_change_party(self):
        Location = Pool().get('stock.location')
        if self.party:
            to_location = Location.search([('id', '=', self.party.customer_location.id)])
            self.to_location = to_location[0]

    @classmethod
    def _group_purchase_key(cls, request):
        """
        The key to group lines by purchases
        A list of key-value as tuples of the purchase
        """
        return (
            ('company', request.requisition.company),
            ('party', request.supplier),
            ('payment_term', request.supplier.supplier_payment_term),
            ('warehouse', request.requisition.from_location.parent),
            ('invoice_address', request.supplier.address_get(type='invoice')),
            )

    @classmethod
    def _group_purchase_line_key(cls, request):
        """
        The key to group requests by lines
        A list of key-value as tuples of the purchase line
        """
        return (
            ('product', request.product),
            ('description', request.comment),
            )

    @classmethod
    def update_stock(cls, records):
        for record in records:
            for line in records.requisition_line:
                pass

    @classmethod
    def update_stock_quantity(cls, records):
        RequisitionLine = Pool().get('requisition.line')
        for record in records:
            RequisitionLine.update_stock_quantity(record.requisition_line)

    @classmethod
    def create_purchase(cls, records):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')
        Date = pool.get('ir.date')
        for req in records:
            today = Date.today()
            requests = [r for r in req.requisition_line if r.state == 'revision' and r.supplier and r.purchase_quantity and r.state_purchase != 'done']
            if requests:
                print('si entra a la compra')
                keyfunc = partial(cls._group_purchase_key)
                requests = sorted(requests, key=sortable_values(keyfunc))
                purchases = []
                lines = []
                for key, grouped_requests in groupby(requests, key=keyfunc):
                    grouped_requests = list(grouped_requests)
                    purchase_date = today
                    purchase = Purchase(purchase_date=purchase_date)
                    for f, v in key:
                        setattr(purchase, f, v)
                    purchases.append(purchase)
                    for line_key, line_requests in groupby(
                            grouped_requests, key=cls._group_purchase_line_key):
                        line_requests = list(line_requests)
                        line = cls.compute_purchase_line(
                            line_key, line_requests, purchase)
                        line.purchase = purchase
                        # line.requests = line_requests
                        print(line.taxes)
                        lines.append(line)
                Purchase.save(purchases)
                Line.save(lines)
                for l in lines:
                    l.taxes = line.compute_taxes(purchase.party)
                Line.save(lines)
                for pur in purchases:
                    print(pur.tax_amount)
            for r in req.requisition_line:
                if r.quotations:
                    for quotation in r.quotations:
                        if quotation.state == 'approved':
                            r.state = 'purchased'
                            r.state_purchase = 'done'
                            r.save()
                            break

            state = req.state
            for r in req.requisition_line:
                if r.state != 'purchased':
                    state = 'approved'
                    break
                else:
                    state = 'done'

            req.state = state
            req.save()

    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        pool = Pool()
        Line = pool.get('purchase.line')

        line = Line()
        # line.unit_price = round_price(Decimal(0))
        for f, v in key:
            setattr(line, f, v)
        line.quantity = sum(r.purchase_quantity for r in requests)
        # line.unit_price = round_price(Decimal(100000))
        line.purchase = purchase
        line.on_change_product()
        # Set again in case on_change's changed them
        for f, v in key:
            setattr(line, f, v)
        line.on_change_quantity()
        line.unit_price = requests[0].quotation_unit_price
        return line

    @classmethod
    def get_move(cls, request):
        """
        Return moves for the requisition
        """
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        quantity = request.delivered_quantity
        quantity = request.uom.round(quantity)
        if quantity <= 0:
            return

        if not request.requisition.to_location:
            pass

        move = Move()
        move.quantity = quantity
        move.uom = request.uom
        move.product = request.product
        print(request.requisition.from_location.output_location)
        print(request.requisition.to_location)
        move.from_location = request.requisition.from_location.output_location
        move.to_location = request.requisition.to_location
        move.state = 'draft'
        move.company = request.requisition.company
        move.unit_price = request.product.cost_price
        move.currency = request.requisition.company.currency
        move.planned_date = Date.today()
        move.origin = (request)
        return move

    @classmethod
    def create_shipment(cls, records):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        Date = pool.get('ir.date')
        active_id = Transaction().context.get('active_id')
        today = Date.today()
        for req in records:
            requests = [r for r in req.requisition_line if req.requisition_line and r.delivered_quantity and r.state_shipment != 'done']

            if requests:

                shipment = Shipment()
                shipment.planned_date = today
                shipment.company = req.company
                shipment.customer = req.party
                shipment.delivery_address = req.party.addresses[0]
                shipment.analytic_account = req.analytic_account
                # shipment.reference = 'Requisicion ' + str(req.id) + req.num_orden if req.num_orden else ''
                shipment.reference = 'Requisicion ' + str(req.id) + req.num_orden if req.num_orden else 'Requisicion ' + str(req.id)
                shipment.warehouse = req.from_location
                moves = []
                for request in requests:
                    moves.append(cls.get_move(request))
                shipment.moves = moves
                Shipment.save([shipment])

                # Update state_shipment of requisition line
                for r in req.requisition_line:
                    print('before if')
                    if req.requisition_line and r.delivered_quantity:
                        print('if')
                        r.state_shipment = 'done'
                        r.save()
                request.state = 'waiting'
            return 'end'

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def waiting(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approved(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('quotation')
    def quote(cls, records):
        pass
        # for record in records:
        #     for lines in record.requisition_line:
        #         if lines.quotations:
        #             pass
        #         else:
        #             raise UserError("You cannot process.", "because the requisition dont have quotations")

    @classmethod
    @ModelView.button
    @Workflow.transition('purchase_approval')
    def purchase_approval(cls):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_requisition_date(cls):
        return datetime.date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    # @classmethod
    # @ModelView.button
    # def button_create_purchase(cls, records):
    #     for record in records:
    #         record.purchase_requisition()


class RequisitionLine(ModelSQL, ModelView, CompanyValueMixin):
    "Requisition Line"
    __name__ = 'requisition.line'

    product = fields.Many2One("product.product", "Product", required=False,

        domain=[('purchasable', '=', True)],
        states=STATES,
        context={
            'company': Eval('company', -1),
            },
        depends=['state', 'company'])
    description = fields.Text('Description',
    states={
        'readonly': ~Eval('state').in_(['draft']),
    },
         depends=['state'])
    comment = fields.Text('Comment',
         depends=DEPENDS)
    required_quantity = fields.Float('Required Quantity', required=True,
        digits=(16, Eval('uom_digits', 2)), depends=DEPENDS + ['uom_digits'])
    delivered_quantity = fields.Float('Delivered Quantity',
        digits=(16, Eval('uom_digits', 2)), depends=DEPENDS + ['uom_digits'], states={'readonly': True})
    stock_quantity = fields.Float('Stock Quantity', digits=(16, Eval('uom_digits', 2)),
        states={'readonly': True})
    purchase_quantity = fields.Float('Purchase Quantity', digits=(16, Eval('uom_digits', 2)), help="You can change this quantity")

    quotation_unit_price = fields.Function(fields.Numeric('Unit Price'),
        'get_info_quotation')

    uom = fields.Many2One('product.uom', 'UOM',
         ondelete='RESTRICT',
         # domain=[
         #     If(Bool(Eval('product_uom_category')),
         #         ('category', '=', Eval('product_uom_category')),
         #         ('category', '!=', -1)),
         #     ],
         states={
             'required': Bool(Eval('product')),
             },
         depends=['product'] + DEPENDS)
    uom_digits = fields.Function(fields.Integer('UOM Digits'),
         'on_change_with_uom_digits')

    cost_price = fields.Numeric('Cost Price', digits=(12, Eval('digits', 2)), depends=['digits'], states={'readonly': True})
    budget_value = fields.Numeric('Budget Value', digits=(16, Eval('digits', 2)), depends=['digits'])
    requisition = fields.Many2One('requisition', 'Requisition Line', required=True)
    quotations = fields.One2Many('requisition.quotation', 'requisition_line', 'Supplier Quotation')
    supplier = fields.Function(fields.Many2One('party.party', 'Supplier Quotation'),
        'get_info_quotation')
    approved_shipment = fields.Selection([
        ('', ''),
        ('rejected', 'Rejected'),
        ('approved', 'Approved'),
        ], 'Approved Shipment')
    purchase_line = fields.Many2One('purchase.line', 'PurchaseProduct', ondelete='CASCADE',
        readonly=True)

    state = fields.Selection([
            ('draft', "Draft"),
            ('revision', "Revision"),
            ('purchased', "Purchased"),
            ('delivered', "Delivered"),
            ('pending', "Pending"),
            ], "State", required=True, readonly=True)

    state_purchase = fields.Selection([
        ('', ''),
        ('pending', "Pending"),
        ('quotated', 'Quotated'),
        ('done', "Done")],
        "State Purchase",
        readonly=True,
    )
    state_shipment = fields.Selection([
        ('', ''),
        ('pending', "Pending"),
        ('done', "Done")],
        "State Shipment",
        readonly=True,
    )
    company = fields.Function(
        fields.Many2One('company.company', "Company"),
        'on_change_with_company')

    @classmethod
    def __setup__(cls):
        super(RequisitionLine, cls).__setup__()
        cls._order[0] = ('id', 'DESC')
        cls._buttons.update({
            'revision_button': {
                'icon': 'tryton-add',
                # 'invisible': Or((Eval('state') != 'purchase_approval'),(Eval('type') != 'purchase'))
                },
            'update_stock_quantity': {
                'icon': 'tryton-add',
                },
            })

    # @classmethod
    # def create(cls, records):
    #     for record in records:
    #         if record.requisition.state != 'draft':
    #             super(RequisitionLine, cls).create(records)
    #         else:
    #             raise UserError('Warning', 'Current state dont permit the action')

    @staticmethod
    def default_required_quantity():
        return 1

    @classmethod
    def default_state(cls):
        return 'draft'

    @fields.depends('uom')
    def on_change_with_uom_digits(self, name=None):
        if self.uom:
            return self.uom.digits
        return 2

    @fields.depends('product', 'stock_quantity', '_parent_requisition.from_location', 'required_quantity')
    def on_change_product(self):
        self.set_values_quantity()

    @fields.depends('product', 'stock_quantity', '_parent_requisition.from_location', 'required_quantity')
    def on_change_required_quantity(self):
        self.set_values_quantity()

    def set_values_quantity(self):
        Date = Pool().get('ir.date')
        if self.product:
            self.uom = self.product.purchase_uom

            if self.requisition and self.requisition.from_location and self.required_quantity:
                context = {
                    'stock_date_end': Date.today(),
                    'locations': [self.requisition.from_location.id],
                    }
                with Transaction().set_context(context):
                    # print(self.product)
                    Product = Pool().get('product.product')
                    product, = Product.search([('id', '=', self.product.id)])
                    quantity = product.quantity
                    # print(quantity,'Cantidad')
                self.stock_quantity = quantity
                self.cost_price = product.cost_price

                if not quantity or quantity < 0:
                    delivered = 0
                # requerido menor que el stock
                elif quantity - self.required_quantity > 0:
                    delivered = self.required_quantity
                    # delivered = quantity - self.required_quantity
                # requerido mayor que el stock
                elif quantity > 0 and self.required_quantity - quantity >= 0:
                    delivered = quantity
                    # pendiente por entregar
                    # purchase = self.required_quantity - quantity

                self.delivered_quantity = delivered
                self.purchase_quantity = self.required_quantity - delivered

    def get_info_quotation(self, name=None):
        res = None
        for line in self.quotations:
            if line.state == 'approved':
                if name == "quotation_unit_price":
                    res = line.amount
                elif name == "supplier":
                    res = line.supplier.id
                break
        return res

    @classmethod
    @ModelView.button
    def update_stock_quantity(cls, records):
        Date = Pool().get('ir.date')
        # ProductShipment = Pool().get('requisition.line')
        # product_shipments = ProductShipment.search([('state', '!=', 'done')])
        for record in records:
            if record.product and record.state_shipment != 'done':
                print(record.id, record.state_shipment)
                record.uom = record.product.purchase_uom
                if record.requisition and record.requisition.from_location and record.required_quantity:
                    context = {
                        'stock_date_end': Date.today(),
                        'locations': [record.requisition.from_location.id],
                        }
                    with Transaction().set_context(context):
                        # print(self.product)
                        Product = Pool().get('product.product')
                        product, = Product.search([('id', '=', record.product.id)])
                        quantity = product.quantity
                        record.stock_quantity = quantity
                        record.save()

    def get_quantity_quotation(self):
        return self.purchase_quantity

    @fields.depends('requisition', '_parent_requisition.company')
    def on_change_with_company(self, name=None):
        if self.requisition and self.requisition.company:
            return self.requisition.company.id

    staticmethod

    def default_state_purchase():
        return 'pending'

    @classmethod
    @ModelView.button
    def revision_button(cls, records):
        for record in records:
            if record.quotations:
                record.state = 'revision'
                record.save()
            else:
                raise UserError("You cannot process.", "because the requisition dont have quotations")


class RequisitionQuotation(Workflow, ModelSQL, ModelView, AttachmentCopyMixin):
    "Requisition Quotation"
    __name__ = 'requisition.quotation'
    supplier = fields.Many2One('party.party', "Supplier", required=True)
    amount = fields.Numeric('Amount', digits=(16, Eval('digits', 2)), required=True, depends=['digits'])
    quantity = fields.Function(fields.Float('Quantity'),
        'get_purchase_quantity')
    budget_value = fields.Function(fields.Numeric('Budget Value'),
        'get_budget_value')
    tax_amount = fields.Function(fields.Numeric('Tax Amount', digits=(16, Eval('digits', 2))),
            'on_change_with_amount')
    total_value = fields.Function(fields.Numeric('Total Value', digits=(16, Eval('digits', 2))),
            'on_change_with_amount')
    # sst_cerificate =  fields.Boolean('SST Certificate')
    # environmental_certificate =  fields.Boolean('Environmental Certificate')
    # basc_certificate =  fields.Boolean('BASC Certificate')

    notes = fields.Char('Notes')
        # states=STATES, depends=DEPENDS)
    state = fields.Selection([
        ('', ''),
        ('revision', 'To Revision'),
        ('approved', "Approved"),
        ('rejected', "Rejected")],
        "State",
        readonly=True,
    )
    requisition_line = fields.Many2One('requisition.line', 'Requisition Line', ondelete='CASCADE',
        required=True)

    purchase_line = fields.Many2One('purchase.line', 'Purchase Product', ondelete='CASCADE',
        )

    file_quotation = fields.Many2One('ir.attachment', 'Attachment')

    @classmethod
    def __setup__(cls):
        super(RequisitionQuotation, cls).__setup__()
        cls._order[0] = ('id', 'DESC')
        cls._transitions |= set((
                ('', 'approved'),
                ('', 'rejected'),
                ('approved', 'rejected'),
                ('rejected', 'approved'),
                ))
        # cls._buttons.update({
        #         'approved_quotation': {
        #             'icon': 'tryton-ok',
        #             # 'invisible': Eval('requisition_line.state_purchase') == 'done',
        #             # 'depends': ['requisition_line.state_purchase'],
        #             },
        #         })

    def get_purchase_quantity(self, name=None):
        return self.requisition_line.purchase_quantity

    def get_budget_value(self, name=None):
        return self.requisition_line.budget_value

    @classmethod
    def default_state(cls):
        return 'revision'

    def on_change_with_amount(self, name):
        res = {'tax_amount': Decimal(0), 'total_value': Decimal(0)}
        Tax = Pool().get('account.tax')
        amount = self.amount
        if amount:
            list_tax = self.requisition_line.product.template.account_category.supplier_taxes
            # print(list_tax,'tax')
            taxes = Tax.compute(list_tax, amount, 1)
            # print(taxes,'taxes')
            res['tax_amount'] = Decimal(sum(v['amount'] for v in taxes)).quantize(Decimal(1) / 10 ** 2)
            res['total_value'] = Decimal(amount + res['tax_amount']).quantize(Decimal(1) / 10 ** 2)
        return res[name]

    # @fields.depends('state')
    # def on_change_state(self):
    #    id = self.id
    #    for quo in self.requisition_line.quotations:
    #        print(quo.id)

    @classmethod
    @ModelView.button
    # @Workflow.transition('approved')
    def approved_quotation(cls, records):
        for record in records:
            record.state = 'approved'
            for line in record.requisition_line.quotations:
                if record.id == line.id:
                    line.state = 'approved'
                    line.save()
                    continue
                line.state = 'rejected'
                line.save()
        return 'reload'


class CreatePurchase(Wizard):
    "Create Purchase"
    __name__ = 'requisition.create_purchase'
    start = StateTransition()

    @classmethod
    def _group_purchase_key(cls, request):
        """
        The key to group lines by purchases
        A list of key-value as tuples of the purchase
        """
        return (
            ('company', request.requisition.company),
            ('party', request.supplier),
            ('payment_term', request.supplier.supplier_payment_term),
            ('warehouse', request.requisition.from_location.parent),
            ('invoice_address', request.supplier.address_get(type='invoice')),
            )

    def _group_purchase_line_key(self, request):
        """
        The key to group requests by lines
        A list of key-value as tuples of the purchase line
        """
        return (
            ('product', request.product),
            ('description', request.comment),
            )

    def transition_start(self):
        pool = Pool()
        Requisition = pool.get('requisition')
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')
        Date = pool.get('ir.date')
        active_id = Transaction().context.get('active_id')

        req = Requisition(active_id)
        today = Date.today()

        requests = [r for r in req.requisition_line if req.requisition_line and r.purchase_quantity and r.supplier and r.state_purchase != 'done']
        if requests:
            # Update state_purchase of requisition line
            for r in req.requisition_line:
                if req.requisition_line and r.purchase_quantity:
                    r.state_purchase = 'done'
                    r.save()

            keyfunc = partial(self._group_purchase_key, requests)
            requests = sorted(requests, key=sortable_values(keyfunc))
            purchases = []
            lines = []
            for key, grouped_requests in groupby(requests, key=keyfunc):
                grouped_requests = list(grouped_requests)
                purchase_date = today
                purchase = Purchase(purchase_date=purchase_date)
                for f, v in key:
                    setattr(purchase, f, v)
                purchases.append(purchase)
                for line_key, line_requests in groupby(
                        grouped_requests, key=self._group_purchase_line_key):
                    line_requests = list(line_requests)
                    line = self.compute_purchase_line(
                        line_key, line_requests, purchase)
                    line.purchase = purchase
                    # line.requests = line_requests
                    print(line.taxes)
                    lines.append(line)
            Purchase.save(purchases)
            Line.save(lines)
            for l in lines:
                l.taxes = line.compute_taxes(purchase.party)
            Line.save(lines)
            for pur in purchases:
                print(pur.tax_amount)

        i = 0
        for r in req.requisition_line:
            if r.state_purchase == 'done':
                i += 1
        if (len(req.requisition_line) == i):
            req.state_purchase = 'done'
            req.save()
        return 'end'

    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        pool = Pool()
        Line = pool.get('purchase.line')

        line = Line()
        # line.unit_price = round_price(Decimal(0))
        for f, v in key:
            setattr(line, f, v)
        line.quantity = sum(r.purchase_quantity for r in requests)
        # line.unit_price = round_price(Decimal(100000))
        line.purchase = purchase
        line.on_change_product()
        # Set again in case on_change's changed them
        for f, v in key:
            setattr(line, f, v)
        line.on_change_quantity()
        line.unit_price = requests[0].quotation_unit_price
        return line


class CreateShipment(Wizard):
    "Create Shipment"
    __name__ = 'requisition.create_shipment'
    start = StateTransition()

    def get_move(self, request):
        """
        Return moves for the requisition
        """
        pool = Pool()
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')

        # quantity = (self._get_move_quantity(shipment_type)
        #     - self._get_shipped_quantity(shipment_type))
        quantity = request.delivered_quantity
        quantity = request.uom.round(quantity)
        if quantity <= 0:
            return

        if not request.requisition.to_location:
            pass
            # raise PartyLocationError(
            #     gettext('sale.msg_sale_customer_location_required',
            #         sale=self.sale.rec_name,
            #         party=self.sale.party.rec_name))
        move = Move()
        move.quantity = quantity
        move.uom = request.uom
        move.product = request.product
        move.from_location = request.requisition.from_location.output_location
        move.to_location = request.requisition.to_location
        move.state = 'draft'
        move.company = request.requisition.company
        # if move.on_change_with_unit_price_required():
        move.unit_price = request.product.cost_price
        move.currency = request.requisition.company.currency
        move.planned_date = Date.today()
        move.origin = (request)
        return move

    def transition_start(self):
        pool = Pool()
        Requisition = pool.get('requisition')
        Shipment = pool.get('stock.shipment.out')
        Move = pool.get('stock.move')
        Date = pool.get('ir.date')
        active_id = Transaction().context.get('active_id')

        req = Requisition(active_id)
        today = Date.today()

        requests = [r for r in req.requisition_line if req.requisition_line and r.delivered_quantity and r.state_shipment != 'done']

        if requests:

            shipment = Shipment()
            shipment.planned_date = today
            shipment.company = req.company
            shipment.customer = req.party
            shipment.delivery_address = req.party.addresses[0]
            shipment.analytic_account = req.analytic_account
            # shipment.customer_location = req.to_location
            # shipment.reference = 'Requisicion ' + str(req.id_interno) + req.num_orden if req.num_orden else ''
            shipment.reference = 'Requisicion ' + str(req.id)
            shipment.warehouse = req.from_location
            moves = []
            for request in requests:
                moves.append(self.get_move(request))
            shipment.moves = moves
            Shipment.save([shipment])

            # Update state_shipment of requisition line
            for r in req.requisition_line:
                print('before if')
                if req.requisition_line and r.delivered_quantity:
                    print('if')
                    r.state_shipment = 'done'
                    r.save()
            request.state = 'waiting'
        return 'end'


class CreateQuotationStart(ModelView):
    "Create Quotation Start"
    __name__ = 'requisition.create_quotation.start'

    supplier = fields.Many2One("party.party", 'Supplier')
    id = fields.Integer('Id', readonly=True)
    name = fields.Char('name')
    file_quotation = fields.Binary("File Quote", help="upload file of quotation supplier", filename='name')
    # file_quotation = fields.Many2One('ir.attachment', 'Attachment')
    products_quote = fields.One2Many('requisition.select_product_ask', 'quotation', 'Product Quote')

    @classmethod
    def __setup__(cls):
        super(CreateQuotationStart, cls).__setup__()

    @staticmethod
    def default_id():
        return 1

    @staticmethod
    def default_name():
        return ' '


class SelectProductAsk(ModelView):
    "Select Product Ask"
    __name__ = 'requisition.select_product_ask'

    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Float('Quantity')
    amount = fields.Numeric('Amount', digits=(16, 2))
    description = fields.Char('Description')
    quotation = fields.Many2One('requisition.create_quotation.start', 'Id')

    @staticmethod
    def default_quotation():
        return 1


class CreateQuotation(Wizard):
    "Create Quotation"
    __name__ = 'requisition.create_quotation'
    start = StateView('requisition.create_quotation.start',
        'requisition.create_quotation_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Quotation', 'create_quotation', 'tryton-ok', default=True),
            ])

    create_quotation = StateTransition()

    def default_start(self, fields):
        active_id = Transaction().context.get('active_id')
        pool = Pool()
        Requisition = pool.get('requisition')
        requisition = Requisition(active_id)
        default = {}
        if requisition:
            products = [
                {
                    'product': l.product.id,
                    'quantity': l.purchase_quantity,
                    'id': 1,
                }
                for l in requisition.requisition_line if l.purchase_quantity > 0
                ]
            default.update({'products_quote': products})
        return default

    def transition_create_quotation(self):
        pool = Pool()
        party = self.start.supplier
        Attachment = pool.get('ir.attachment')
        active_id = Transaction().context.get('active_id')
        file_ = self.start.file_quotation
        RequisitionLine = pool.get('requisition.line')
        lines = RequisitionLine.search([('requisition', '=', active_id)])
        RequisitionQuotation = pool.get('requisition.quotation')
        Requisition = pool.get('requisition')
        requisition = Requisition.search([('id', '=', active_id)])
        req = requisition[0]
        if req.state in ['draft', 'waiting', 'rejected', 'cancelled', 'done', 'purchase_approval', 'purchase']:
            raise UserError('Warning', 'Current state dont permit the action')
        else:
            attachment = None
            if file_:
                attachment = Attachment(
                    name=self.start.name,
                    type='data',
                    data=file_,
                    resource=req,
                )
                Attachment.save([attachment])
            for line, product in zip(lines, self.start.products_quote, strict=False):
                reqQuotation = RequisitionQuotation(
                       requisition_line=line.id,
                       supplier=party.id,
                       amount=product.amount,
                       notes=product.description,
                       file_quotation=attachment.id if attachment else None,
                   )
                RequisitionQuotation.save([reqQuotation])
        return 'end'
