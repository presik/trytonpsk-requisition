
from trytond.pool import Pool, PoolMeta

class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        origins = super(Move,cls)._get_origin()
        origins.extend(['requisition.line'])
        'Return list of Model names for origin Reference'
        return origins
