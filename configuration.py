# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pyson import Id
from trytond import backend
from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool

class Configuration(ModelSQL, ModelView):
    'Requisition Configuration'
    __name__ = 'requisition.configuration'

    company = fields.Many2One('company.company', 'Company', required=True)
    requisition_sequence = fields.Many2One(
            'ir.sequence', "Requisition Sequence", required=True,
            domain=[
                ('company', 'in', [Eval('context', {}).get('company', -1),
                        None]),
                # ('sequence_type', '=',
                #     Id('requisition', 'sequence_type_requisition')),
                ])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_configuration(cls):
        res = cls.search([
            ('company', '=', Transaction().context.get('company'))
        ])
        return res[0]
